extractPoints <- function(filePoints,lonLatSelect=NULL,id=NULL)
{
  library(data.table)
  nameLon = c('lon', 'longitude', 'Longitude', 'LON', 'LongitudeD','x', 'X', 'long')
  nameLat = c('lat', 'latitude', 'Latitude', 'LAT', 'LatitudeD','y', 'Y')
  
  inputDataPts = fread(filePoints)
  
  nlon = intersect(colnames(inputDataPts),nameLon)
  nlat = intersect(colnames(inputDataPts),nameLat)
  if ( is.null(lonLatSelect) && is.null(id) ) {
    inputDataPts = data.frame(inputDataPts[[nlon]], inputDataPts[[nlat]] ) #data.frame( inputDataPts$lon, inputDataPts$lat )
    colnames(inputDataPts)=c('lon','lat')
  } else if ( is.null(lonLatSelect) ) {
    inputDataPts = data.frame(inputDataPts[[id]], inputDataPts[[nlon]], inputDataPts[[nlat]]) #data.frame( inputDataPts$lon, inputDataPts$lat )
    colnames(inputDataPts)=c('id','lon','lat')
  } else if ( is.null(id) ) {
    inputDataPts = data.frame(inputDataPts[[nlon]], inputDataPts[[nlat]] , inputDataPts[[lonLatSelect[1]]])
    colnames(inputDataPts)=c('lon','lat',lonLatSelect[1])
    inputDataPts = inputDataPts[inputDataPts[[lonLatSelect[1]]]==lonLatSelect[2],]
  } else {
    inputDataPts = data.frame(inputDataPts[[id]], inputDataPts[[nlon]], inputDataPts[[nlat]] , inputDataPts[[lonLatSelect[1]]])
    colnames(inputDataPts)=c('id', 'lon','lat',lonLatSelect[1])
    inputDataPts = inputDataPts[inputDataPts[[lonLatSelect[1]]]==lonLatSelect[2],]
  }
  # On retire les lignes ou il y a au moins un NA
  selectPoints = inputDataPts[rowSums(is.na(inputDataPts)) <= 0,]
  return(selectPoints)
}

# Select or/and load point of interest
convertInPoints <- function(workPoints,lonLatSelect=NULL,id=NULL)
{
  if ( is.character(workPoints) ) {
    selectPoints = extractPoints(workPoints,lonLatSelect,id)
  } else if (is.list(workPoints)) {
    ilon = vector()
    ilat = vector()
    for ( i in 1:length(workPoints) ) {
      ilon[i] = workPoints[[i]][1]
      ilat[i] = workPoints[[i]][2]
    }
    selectPoints = data.frame(lon = ilon, lat = ilat)
  } else if (is.numeric(workPoints)  && length(workPoints)==2) {
    selectPoints = data.frame(lon = workPoints[1], lat = workPoints[2])
  } else {
    stop('The indicate work points are not extractable from ', workPoints, '!')
  }
  return(selectPoints)
}

selectMNT <- function(selectPoint, mntDIR)
{
  folders = strsplit(mntDIR,'/')[[1]]
  mntType = folders[length(folders)]
  
  if ( mntType == 'GMTED2010_7.5' || mntType =='GMTED2010_15' || mntType =='GMTED2010_30') {
    if ( mntType == 'GMTED2010_7.5' ) {
      postfix= "_20101117_gmted_mea075.tif"
    } else if ( mntType =='GMTED2010_15') {
      postfix = "_20101117_gmted_mea150.tif"
    } else if ( mntType =='GMTED2010_30' ) {
      postfix = "_20101117_gmted_mea300.tif"
    }
    prefix = ""
    fmtlonlat = c('n','s','e','w')
    fmtLat ="%02d"
    resLat = 20
    fmtLon = "%03d"
    resLon = 30
  } else if ( mntType =='SRTM_1arc_Europe') {
    postfix= "_1arc_v3.bil"
    prefix = "fichier_qgis/"
    fmtlonlat = c('n','s','e','w')
    fmtLat ="%02d"
    resLat = 20
    fmtLon = "%03d"
    resLon = 30
    } else {
    stop("The DEM (MNT) used is not yet implemented (to give the good folder name with coordonate).")
  }
  
  nlon = intersect(colnames(selectPoint),c('lon', 'longitude', 'Longitude', 'LON', 'LongitudeD','x'))
  nlat = intersect(colnames(selectPoint),c('lat', 'latitude', 'Latitude', 'LAT', 'LatitudeD','y'))
  
  if ( mntType == 'GMTED2010_7.5' || mntType =='GMTED2010_15' || mntType =='GMTED2010_30' )
  {
    for ( i in seq(90,-89,by=-resLat) ) {
      if ( selectPoint[[nlat]] < i ) ilat = i - resLat
    }
    for ( j in seq(180,-179,by=-resLon) ) {
      if ( selectPoint[[nlon]] < j ) ilon = j - resLon
    }
  } else if ( mntType =='SRTM_1arc_Europe' ) {
    ilat = trunc(selectPoint[[nlat]])
    if (selectPoint[[nlat]]<0) ilat = ilat - 1
    ilon = trunc(selectPoint[[nlon]])
    if (selectPoint[[nlon]]<0) ilon = ilon - 1
    if ( ilat >= 60 ) cat(paste0('WARNING: ',mntType,' do not have data for latitude >= 60°N.\n'))
  }
  
  if ( selectPoint[[nlon]] >= 0 ){
    ilonEW = fmtlonlat[3]
  } else {
    ilonEW = fmtlonlat[4]
  }
  
  if ( selectPoint[[nlat]] >= 0 ){
    ilatNS = fmtlonlat[1]
  } else {
    ilatNS = fmtlonlat[2]
  }
  
  if ( mntType == 'GMTED2010_7.5' || mntType =='GMTED2010_15' || mntType =='GMTED2010_30' ) {
    MNT_file = file.path(mntDIR, paste0(prefix,sprintf(fmtLat, abs(ilat)),ilatNS,sprintf(fmtLon, abs(ilon)),ilonEW,postfix))
  } else if ( mntType =='SRTM_1arc_Europe' ) {
    MNT_file = file.path(mntDIR, paste0(prefix,ilatNS,sprintf(fmtLat, abs(ilat)),'_',ilonEW,sprintf(fmtLon, abs(ilon)),postfix))
  }
  
  if (!file.exists(MNT_file)) stop(paste0('The file ',MNT_file,' does not exist. Please check.'))
  
  return(MNT_file)
}

locPointName <- function(selectPoint, latlonDigit)
{
  nameLon = c('lon', 'longitude', 'Longitude', 'LON', 'LongitudeD','x', 'X', 'long')
  nameLat = c('lat', 'latitude', 'Latitude', 'LAT', 'LatitudeD','y', 'Y')
  nlon = intersect(names(selectPoint),nameLon)
  nlat = intersect(names(selectPoint),nameLat)
  
  if ( selectPoint[nlon] >= 0 ){
    ilonEW = 'E'
  } else {
    ilonEW = 'W'
  }
  ilon = paste0(sprintf(latlonDigit, abs(selectPoint[nlon])),ilonEW)
  
  if ( selectPoint[nlat] >= 0 ){
    ilatNS = 'N'
  } else {
    ilatNS = 'S'
  }
  ilat = paste0(sprintf(latlonDigit, abs(selectPoint[nlat])),ilatNS)
  return(paste0(ilon,'-',ilat))
}

# #Routines de conversion d'énergie en puissance 
# energy2powerConvert <- function(periode_integ, energy_unit) # periode_integ in second
# {
#   if(energy_unit == "Wh/m2") # /4.57 umol/cm2 ==> to Wh/m2
#   {
#     energy2power = 3600*(1/periode_integ)
#     
#   } else if (energy_unit == "J/cm2")
#   {
#     energy2power = 10000*(1/periode_integ)
#     
#   } else if(energy_unit == "J/m2")
#   {
#     energy2power = (1/periode_integ)
#     
#   } else if(energy_unit == "MJ/m2")
#   {
#     energy2power = (1/periode_integ)*10^6
#     
#   } else {
#     stop('You do not have given a compatible unit of energy:',energy_unit)
#   }
#   return(energy2power)
# }

#Routines de conversion d'énergie en puissance 
energy2powerConvert <- function(periode_integ, energy_unit) # Conversion en joules sur la période étudiée
{
  if(energy_unit == "Ws/m2" || energy_unit == "J/m2") {
    energy2power = 1
  } else if (energy_unit == "Wh/m2") {
    energy2power = 3600
  } else if (energy_unit == "J/cm2") {
    energy2power = 10000
  }  else if(energy_unit == "MJ/m2") {
    energy2power = 10^6
  } else if(energy_unit == "PPFD") { # Photosynthetic Photon Flux Density, in μmol quanta m−2 s−1
    # A. # 1 μmol quanta m−2 s−1 ==> 0.217 Watts m−2 (Timothy J.B. Carruthers, ... Keiko Aioi, in Global Seagrass Research Methods, 2001) -> Chapter 19 - Measurement of light penetration in relation to seagrass   https://doi.org/10.1016/B978-044450891-1/50020-7
    # Table 19-1. Approximate conversion factors for different units of measure used for light – assuming light of 400-700 nm in all cases (a. Sunlight after Morel and Smith 1974, McCluney 1994, Valiela 1995, b. Artificial light after Hall 1993, Dawes 1998)
    # https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/photosynthetically-active-radiation
    # B. # https://en.wikipedia.org/wiki/Photosynthetically_active_radiation
    # with T = 5800 Kelvin (daylight) ηphoton (μmol/J* or μmol s−1W*−1) = 4.56 ==> = * (1/4.56)
    # C. # ##1/4.57   # Nicos: Obs en umol de photon /m2/s
    energy2power = 3600 * 0.217      
  } else {
    stop('You do not have given a compatible unit of energy:',energy_unit)
  }
  return(energy2power/periode_integ)
}

cropTop <- function(r,coords)
{
  library(terra)
  # To realy take all studied zone
  if ( length(coords)!=4 ) stop("c(xmin,xmax,ymin,ymax) coordonate needed!")
  coords[1] = xmin(r)+ceiling(round(abs(xmin(r)-coords[1])/res(r)[1],5))*res(r)[1] # Here we take the points inside. For the point just ouside, change ceiling <-> floor
  coords[2] = xmin(r)+floor(round(abs(xmin(r)-coords[2])/res(r)[1],5))*res(r)[1]
  coords[3] = ymin(r)+ceiling(round(abs(ymin(r)-coords[3])/res(r)[2],5))*res(r)[2]
  coords[4] = ymin(r)+floor(round(abs(ymin(r)-coords[4])/res(r)[2],5))*res(r)[2]
  return(crop(r, coords))
}