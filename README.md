# ModelData_Toolkit


## Description

Multiple scripts to download, manipulate, and modify data used as input for environmental modeling. This includes climatic data, LAI, soil characteristics, and more.


## Getting started

If you want to add a new program, all files must begin with a **common suffix followed by "_" and the name of the file itself** (e.g. TAWinv_functions.R).
All data have to be storage in the "DATA" folder.


## Programmes included in this project (and brief description)

- [ ] ** RadDownscaling:** Method to downscale global radiation (e.g. from ERA5_Land) at higher resolution using a digital elevation model (DEM), particularly usefull in complex terrain such as montains.
