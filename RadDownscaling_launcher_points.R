#!/usr/bin/env R

# -------------------------------------------------------------------
#  Author:      Arsene Druel
#  Email :      arsene.druel@gmail.com
#  Created:     2022-03-02
#  Updated:     2024-06-12
# -------------------------------------------------------------------

################################################################################################
################################# Correction of the radiations! ################################
#################################   LAUNCHER SCRIPT ON POINTS   ################################
################################################################################################

# CONFIGURATIONS #
##################

# Pathenames
mainfFolder   <- file.path(path.expand('~'),"ModelData_Toolkit-GIT")
script_FOLDER <- mainfFolder
dem_FOLDER    <- file.path(path.expand('~'),"/data/mnt") # You have to download it ! E.g. GMTED2010 or SRTM on https://earthexplorer.usgs.gov/ 
input_FOLDER  <- file.path(mainfFolder,"DATA/RadDownscaling/Ventoux/ERA5_land_SurEau_hourly")
output_FOLDER <- file.path(mainfFolder,"OUTPUT/RadDownscaling/Ventoux/ERA5_land_SurEau_hourly")

# For outFiles
add_name_suffix = ""

# Points #
# Can be a point c(lon,lat), a list of points, or a link to a file (.csv)
workPoints   <- file.path(mainfFolder,"DATA/RadDownscaling/Ventoux/ListeSite.csv")
idigit       <- 3      # Number of numbers after degree de specify a point. Use to name output files
lonLatSelect <- NULL   # in case of table.csv, it is possible to select an attribute from a column c("colName","value")

# Years of corrections
strt_year <- 2016
stop_year <- 2017

# list of DEM used for correction
list_DEM        <- c("GMTED2010_7.5","GMTED2010_15","GMTED2010_30","SRTM_1arc_Europe")
list_degradeDEM <- 1 # default = 1 for no degradation. To reduce the resolution of DEM you can indicate list c(2,3,4,6,8,16)

# Climate files
inputName          <- 'ERA5_land' # Climate file prefix
idigit_climateData <- 1           # number of decimal places used for climate files


# Optional configuration #
##########################

# select the method of the correction (can be a list)  
splitRG_methodS = "Spitters" # c("Spitters", "Roderick", "Bird")

# Input file config
file_type = "csv_with_col_name" # "soda", "hourly_station", "daily_station", "csv_with_col_name"
#- Initial data units, "Wh/m2", "J/cm2" ou bien "J/m2". ou "MJ/m2" (10^6)
energy_unit = "MJ/m2" 

# DEM configuration
DEMresUnit='degree' #'degree' or 'm'


# SCRIPT #
##########

if ( TRUE ) {
  if (!dir.exists(output_FOLDER)) dir.create(output_FOLDER, recursive = TRUE)
  
  source(file.path(script_FOLDER,'commonFunctionv3.R'))
  source(file.path(script_FOLDER,'RadDownscaling_functions.R'))
  
  #Initialisation of points (load if necessary)
  selectPoints = convertInPoints(workPoints,lonLatSelect)

  if ( FALSE ) { # to import information from MASK_POINTS_XYZ.csv (point, elevation...)
    MASK_ERA = fread(file.path(input_FOLDER,"MASK_POINTS_XYZ.csv"))
    listPts <- NULL
    for ( i in c(1:dim(selectPoints)[1]) ) {
      listPts = rbind(listPts, MASK_ERA[MASK_ERA$x == selectPoints[i,'lon'] & MASK_ERA$y == selectPoints[i,'lat'],3:dim(MASK_ERA)[2]][1,])
    }
    selectPoints = cbind(selectPoints, listPts)
  } else {
    iptsName <- NULL
    for ( ipts in c(1:nrow(selectPoints)) ) {
      iptsName = c(iptsName, locPointName(selectPoints[ipts,c('lon','lat')],paste0("%.",idigit_climateData,"f")))
    } 
    selectPoints = cbind(selectPoints, climPts = iptsName)
  }
  
  

  # LAUNCH CORRECTION on points !#
  ################################
  for ( DEMname in list_DEM ) {
    cat("START DEM",DEMname,"\n")
    
    # Check DEM
    if ( DEMname =='SRTM_1arc_Europe' && any(selectPoints[intersect(colnames(selectPoints),c('lat', 'latitude', 'Latitude', 'LAT', 'LatitudeD','y'))]>=60) ) {
      cat('WARNING: You have choosen the DEM SRTM_1arc_Europe and assoiciated some point >=60°N. They are ignored.')
      selectPoints=selectPoints[selectPoints[intersect(colnames(selectPoints),c('lat', 'latitude', 'Latitude', 'LAT', 'LatitudeD','y'))]<60,]
    }
    
    for ( splitRG_method in splitRG_methodS ) {
      cat("--> With split method ",splitRG_method,"\n")
      
      add_save_name=paste0('_',splitRG_method,add_name_suffix)
      
      for ( degradeDEM in list_degradeDEM ) {
        if (length(list_degradeDEM)>1) cat("----> With degradation of DEM =",degradeDEM,"\n")
        if ( degradeDEM==1 ) degradeDEM <- NULL
        if ( !is.null(degradeDEM) ) DEM_name = paste(DEMname,degradeDEM,sep='_dgr') else DEM_name = DEMname
      
        for ( ipts in c(1:nrow(selectPoints)) ) {
          # select DEM
          DEM_file  = selectMNT(selectPoints[ipts,c('lon','lat')], file.path(dem_FOLDER,DEMname))
          
          file_path = file.path(input_FOLDER,paste0(inputName,'_',selectPoints[ipts,'climPts'],'_',strt_year,'-',stop_year,'.csv'))
          
          radCorrect = radiationDownscaling(selectPoints[ipts,c('lon','lat')], strt_year, stop_year, file_path, file_type, energy_unit, DEM_file, DEMresUnit,
                                            method = splitRG_method, coefMethod = "weighting", solarIntensity = TRUE, timeStep = 3, diffuMethod = 'computeAngle', nbRadIn = 500, min_z = 0, degradeDEM = degradeDEM, nbCores = 8) #nbCores: 2 ans: opt 4, max 8
          
          file_path_out = file.path(output_FOLDER,paste0('radCor_',locPointName(selectPoints[ipts,c('lon','lat')],paste0("%.",idigit,"f")),'_',strt_year,'-',stop_year,'_',DEM_name,add_save_name,'.csv'))
          fwrite(radCorrect, file_path_out)
          cat ("Point",ipts,"on",nrow(selectPoints),"done.\n")
        }
      }
    }
  }
  cat("The script is finished!\n")
  
}
